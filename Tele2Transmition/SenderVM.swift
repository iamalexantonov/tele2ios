//
//  SenderVM.swift
//  Tele2Transmition
//
//  Created by Alexey Antonov on 23/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import Foundation
import Network

final class SenderVM: ObservableObject {
    @Published var results = [NWBrowser.Result]()
    
    init() {
        clear()
        
        if sharedBrowser == nil {
            sharedBrowser = PeerBrowser(delegate: self)
        }
    }
    
    func connect(to: NWBrowser.Result) {
        if sharedConnection == nil {
            sharedConnection = PeerConnection(endpoint: to.endpoint,
            interface: to.interfaces.first,
            passcode: "0000",
            delegate: self)
        }
    }
}

extension SenderVM: PeerConnectionDelegate {
    func connectionReady() {
        print("Connected")
    }
    
    func connectionFailed() {
        print("Ой")
    }
    
    func receivedMessage(content: Data?, message: NWProtocolFramer.Message) {
        //sharedConnection?.cancel()
    }
    
    func displayAdvertiseError(_ error: NWError) {
        print(error.localizedDescription)
    }
    
}

extension SenderVM: PeerBrowserDelegate {
    func refreshResults(results: Set<NWBrowser.Result>) {
         for result in results {
             if case let NWEndpoint.service(name: name, type: _, domain: _, interface: _) = result.endpoint {
                if name != deviceName {
                 self.results.append(result)
               }
             }
         }
     }
     
     func displayBrowseError(_ error: NWError) {
         print(error.localizedDescription)
     }
}
