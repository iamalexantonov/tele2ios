//
//  DancingCircles.swift
//  Tele2Transmition
//
//  Created by Alexey Antonov on 23/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import SwiftUI

struct DancingCircles: View {
    @State private var moveOn = true
    var body: some View {
        ZStack {
            Circle()
                .opacity(moveOn ? 0.77 : 0.33)
                .scaleEffect(moveOn ? 1 : 2)
                .frame(width: 90, height: 90)
                .animation(Animation.linear(duration: 4).repeatForever(autoreverses: true))
                .offset(x: moveOn ? 0 : 0, y: moveOn ? -10 : 0)
                .onAppear(){ self.moveOn.toggle() }
            Circle()
                .opacity(moveOn ? 0.77 : 0.33)
                .scaleEffect(moveOn ? 1 : 2)
                .frame(width: 90, height: 90)
                .animation(Animation.linear(duration: 4).repeatForever(autoreverses: true))
                .offset(x: moveOn ? -30 : 0, y: moveOn ? 10 : 0)
                .onAppear(){ self.moveOn.toggle() }
            Circle()
                .opacity(moveOn ? 0.77 : 0.33)
                .scaleEffect(moveOn ? 1 : 2)
                .frame(width: 90, height: 90)
                .animation(Animation.linear(duration: 4).repeatForever(autoreverses: true))
                .offset(x: moveOn ? 20 : 0, y: moveOn ? 20 : 0)
                .onAppear(){self.moveOn.toggle()}
        }
    }
}

struct DancingCircles_Previews: PreviewProvider {
    static var previews: some View {
        DancingCircles()
    }
}
