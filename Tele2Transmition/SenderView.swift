//
//  SenderView.swift
//  Tele2Transmition
//
//  Created by Alexey Antonov on 23/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import SwiftUI
import Network

struct SenderView: View {
    var dataType: DataType
    var amount: Int
    
    var textOfType : String {
        if dataType == .internet {
            return "мин"
        } else {
            return "Гб"
        }
    }
    
    @ObservedObject private var vm = SenderVM()
    
    var body: some View {
        VStack {
            List(vm.results, id: \.self) { result in
                Button(action: {
                    self.vm.connect(to: result)
                    sharedConnection?.sendMessage("Абонент \(deviceName) Отправлено \(self.amount) \(self.textOfType)", callback: {})
                }, label: {
                    Text(getDeviceName(of: result))
                })
            }
        }.navigationBarTitle("Отправить \(amount) \(textOfType)")
    }
}

struct SenderView_Previews: PreviewProvider {
    static var previews: some View {
        SenderView(dataType: .internet, amount: 100)
    }
}
