import Network

var sharedListener: PeerListener?

class PeerListener {

	weak var delegate: PeerConnectionDelegate?
	var listener: NWListener?
	var name: String
	let passcode: String

	init(name: String, passcode: String, delegate: PeerConnectionDelegate) {
		self.delegate = delegate
		self.name = name
		self.passcode = passcode
		startListening()
	}

	func startListening() {
		do {
			let listener = try NWListener(using: NWParameters(passcode: passcode))
			self.listener = listener
			listener.service = NWListener.Service(name: self.name, type: "_tictactoe._tcp")

			listener.stateUpdateHandler = { newState in
				switch newState {
				case .ready:
					print("Listener ready on \(String(describing: listener.port))")
				case .failed(let error):
					if error == NWError.dns(DNSServiceErrorType(kDNSServiceErr_DefunctConnection)) {
						print("Listener failed with \(error), restarting")
						listener.cancel()
						self.startListening()
					} else {
						print("Listener failed with \(error), stopping")
						self.delegate?.displayAdvertiseError(error)
						listener.cancel()
					}
				case .cancelled:
					sharedListener = nil
				default:
					break
				}
			}

			listener.newConnectionHandler = { newConnection in
				if let delegate = self.delegate {
					if sharedConnection == nil {
						sharedConnection = PeerConnection(connection: newConnection, delegate: delegate)
					} else {
						newConnection.cancel()
					}
				}
			}

			listener.start(queue: .main)
		} catch {
			print("Fail")
			abort()
		}
	}

	func resetName(_ name: String) {
		self.name = name
		if let listener = listener {
			listener.service = NWListener.Service(name: self.name, type: "_tictactoe._tcp")
		}
	}
}
