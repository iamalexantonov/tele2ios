import Foundation
import Network

var sharedConnection: PeerConnection?

protocol PeerConnectionDelegate: class {
	func connectionReady()
	func connectionFailed()
	func receivedMessage(content: Data?, message: NWProtocolFramer.Message)
	func displayAdvertiseError(_ error: NWError)
}

class PeerConnection {

	weak var delegate: PeerConnectionDelegate?
	var connection: NWConnection?
	let initiatedConnection: Bool

	init(endpoint: NWEndpoint, interface: NWInterface?, passcode: String, delegate: PeerConnectionDelegate) {
		self.delegate = delegate
		self.initiatedConnection = true

		let connection = NWConnection(to: endpoint, using: NWParameters(passcode: passcode))
		self.connection = connection

		startConnection()
	}

	init(connection: NWConnection, delegate: PeerConnectionDelegate) {
		self.delegate = delegate
		self.connection = connection
		self.initiatedConnection = false

		startConnection()
	}

	func cancel() {
		if let connection = self.connection {
			connection.cancel()
			self.connection = nil
		}
	}

	func startConnection() {
		guard let connection = connection else {
			return
		}

		connection.stateUpdateHandler = { newState in
			switch newState {
			case .ready:
				print("\(connection) established")

				self.receiveNextMessage()

				if let delegate = self.delegate {
					delegate.connectionReady()
				}
			case .failed(let error):
				print("\(connection) failed with \(error)")

				connection.cancel()

				if let delegate = self.delegate {
					delegate.connectionFailed()
				}
			default:
				break
			}
		}

		connection.start(queue: .main)
	}

    func sendMessage(_ info: String, callback: @escaping ()->()) {
		guard let connection = connection else {
			return
		}

		let message = NWProtocolFramer.Message(messageType: .move)
		let context = NWConnection.ContentContext(identifier: "Move",
												  metadata: [message])

        connection.send(content: info.data(using: .unicode), contentContext: context, isComplete: true, completion: .contentProcessed({ error in
            if let error = error {
                print(error.localizedDescription)
            } else {
                callback()
            }
        }))
	}

	func receiveNextMessage() {
		guard let connection = connection else {
			return
		}

		connection.receiveMessage { (content, context, isComplete, error) in
			if let message = context?.protocolMetadata(definition: AppProtocol.definition) as? NWProtocolFramer.Message {
				self.delegate?.receivedMessage(content: content, message: message)
			}
			if error == nil {
				self.receiveNextMessage()
			}
		}
	}
}
