//
//  RecieverView.swift
//  Tele2Transmition
//
//  Created by Alexey Antonov on 23/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import SwiftUI

struct RecieverView: View {
    @ObservedObject private var vm = RecieverVM()
    var body: some View {
        VStack {
            Text("Идёт получение...").font(Font.custom("SF Compact Display Heavy", size: 28))
            Spacer()
            DancingCircles()
            Spacer()
        }.onAppear(perform: {
            self.vm.setUp()
        }).onDisappear(perform: {
            clear()
        })
        .alert(isPresented: $vm.messageRecieved, content: {
            Alert(title: Text(self.vm.message), message: nil, dismissButton: Alert.Button.default(Text("OK"), action: {
                clear()
            }))
        })
    }
}

struct RecieverView_Previews: PreviewProvider {
    static var previews: some View {
        RecieverView()
    }
}
