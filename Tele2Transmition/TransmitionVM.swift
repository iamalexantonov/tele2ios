//
//  TransmitionVM.swift
//  Tele2Transmition
//
//  Created by Alexey Antonov on 22/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import Foundation
import Network
import UIKit

final class TransmitionVM: ObservableObject {
    @Published var message = "" {
        didSet {
            messageRecieved = message == "" ? false : true
        }
    }
    @Published var messageRecieved = false
    @Published var results = [NWBrowser.Result]()
    @Published private(set) var controlsEnabled = true
    @Published private(set) var tableVisible = false
    
    func setReadingMode() {
        clear()
        controlsEnabled = false
        sharedListener = PeerListener(name: deviceName, passcode: "0000", delegate: self)
    }
    
    

}

extension TransmitionVM: PeerConnectionDelegate {
    func connectionReady() {
        sharedConnection?.sendMessage("Абонент \(deviceName) отправил вам 1 Гб") {
            clear()
        }
    }
    
    func connectionFailed() {
        print("Fail")
    }
    
    func receivedMessage(content: Data?, message: NWProtocolFramer.Message) {
        if let content = content, let messageString = String(data: content, encoding: .unicode) {
            if !messageString.contains(deviceName) {
                self.message = messageString
            }
        }
    }
    
    func displayAdvertiseError(_ error: NWError) {
        var message = "Error \(error)"
        if error == NWError.dns(DNSServiceErrorType(kDNSServiceErr_NoAuth)) {
            message = "Not allowed to access the network"
        }
        print(message)
    }
    
    
}
