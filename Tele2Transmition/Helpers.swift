//
//  Helpers.swift
//  Tele2Transmition
//
//  Created by Alexey Antonov on 23/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import SwiftUI
import Network
import UIKit

let deviceName = UIDevice.current.name

func getDeviceName(of: NWBrowser.Result) -> String {
    if case let NWEndpoint.service(name: name, type: _, domain: _, interface: _) = of.endpoint {
        return name
    } else {
        return "Неизвестная зверушка"
    }
}

extension Color {
    static let offWhite = Color(red: 225 / 255, green: 225 / 255, blue: 235 / 255)
}

struct SimpleButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
        .padding(30)
        .background(
            Group {
                if configuration.isPressed {
                    Circle()
                    .fill(Color.offWhite)
                    .overlay(
                        Circle()
                            .stroke(Color.gray, lineWidth: 4)
                            .blur(radius: 4)
                            .offset(x: 2, y: 2)
                            .mask(Circle().fill(LinearGradient(Color.black, Color.clear)))
                    )
                    .overlay(
                        Circle()
                            .stroke(Color.white, lineWidth: 8)
                            .blur(radius: 4)
                            .offset(x: -2, y: -2)
                            .mask(Circle().fill(LinearGradient(Color.clear, Color.black)))
                    )
                } else {
                    Circle()
                        .fill(Color.offWhite)
                        .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                        .shadow(color: Color.white.opacity(0.7), radius: 10, x: -5, y: -5)
                }
            }
        )
    }
}


extension LinearGradient {
    init(_ colors: Color...) {
        self.init(gradient: Gradient(colors: colors), startPoint: .topLeading, endPoint: .bottomTrailing)
    }
}

func clear() {
    sharedConnection?.cancel()
    sharedConnection = nil
    sharedBrowser = nil
    sharedListener = nil
}
