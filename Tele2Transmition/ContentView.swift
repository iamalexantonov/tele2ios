//
//  ContentView.swift
//  Tele2Transmition
//
//  Created by Alexey Antonov on 22/08/2020.
//  Copyright © 2020 Alex Antonov. All rights reserved.
//

import SwiftUI

enum DataType {
    case mobile
    case internet
}

struct MainScreen: View {
    @State private var amount: Int = 20
    @State private var typeOfData = DataType.internet
    
    var heightOfScreen = UIScreen.main.bounds.height
    var shortScreen = Int((UIScreen.main.bounds.height - 200)/UIScreen.main.bounds.height)
    
    @State private var amountOfInternet = 100
    @State private var amountOfMinutes = 2000
    
    var textOfType : String {
        if typeOfData == .internet {
            return "Минуты"
        } else {
            return "Гигабайты"
        }
    }
    var body: some View {
        GeometryReader { geo in
            ZStack {
                Rectangle()
                    .foregroundColor(Color.clear)
                    
                VStack(alignment: .center) {
                    Group {
                        HStack {
                            ZStack {
                                HStack(alignment: .center) {
                                    Spacer(minLength: UIScreen.main.bounds.width*0.4)
                                    
                                    Text(deviceName)
                                        .font(Font.custom("SF Compact Display Light", size: 25))
                                    
                                    Spacer(minLength: UIScreen.main.bounds.width*0.2)
                                    
                                }
                            }
                            Spacer()
                        }
                        Spacer()
                        Text(self.textOfType)
                            .font(Font.custom("SF Compact Display Light", size: 50))
                            .gesture(TapGesture()
                                .onEnded() {
                                    if self.typeOfData == DataType.mobile {self.typeOfData = DataType.internet}
                                    else {self.typeOfData = DataType.mobile}
                                }
                        )
                        Spacer()
                        Text("\(self.amount)")
                            .font(Font.custom("SF Compact Display Heavy", size: 100))
                        Spacer()
                        Spacer()
                        NavigationLink(destination: SenderView(dataType: self.typeOfData, amount: self.amount), label: {
                            ZStack {
                                Circle()
                                .fill(Color.offWhite)
                                .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                                .shadow(color: Color.white.opacity(0.7), radius: 10, x: -5, y: -5)
                                Image(systemName: "paperplane.fill")
                                .padding()
                            }
                        }).foregroundColor(.gray)
                        .frame(width: 60, height: 60)
                        //.buttonStyle(SimpleButtonStyle())
                        
                        
                    }
                }
                .gesture(
                    DragGesture()
                        .onChanged({ (value) in
                            self.amount = self.amountOfInternet/2 - Int(value.translation.height) *  self.amountOfInternet / Int(self.heightOfScreen)
                        })
                )
            }
            
        }
    }
}

struct ContentView: View {
    
    var body: some View {
        NavigationView {
            VStack {
                HStack {
                    Text("    ")
                    Image("TELEPASS")
                    Spacer()
                }
                Spacer()
                HStack {
                    NavigationLink(destination: MainScreen(), label: {
                        Text("Отправить").font(.subheadline)
                    }).padding()
                    NavigationLink(destination: RecieverView(), label: {
                        Text("Получить").font(.subheadline)
                    }).padding()
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
